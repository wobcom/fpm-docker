FROM centos:7

RUN yum groupinstall -y 'Development Tools'\
    && yum install -y ruby ruby-devel \
    && gem install --no-ri --no-rdoc fpm

